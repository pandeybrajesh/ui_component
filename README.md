# Create Transparent Drop Down Navigation Menu with CSS and HTML #

 learn how to create a transparent navigation menu with CSS and HTML 

### HTML ###
<!DOCTYPE html>
<html>
<head>
	<title>Transpresnet Mega menu</title>
</head>
<link rel="stylesheet" type="text/css" href="style.css">
<body>
	<ul>
		<li><a href="#">Home</a></li>
		<li><a href="#">About</a>
			<ul>
				<li><a href="#">First</a></li>
				<li><a href="#">Second</a></li>
				<li><a href="#">Third</a></li>
				<li><a href="#">Four</a></li>
				<li><a href="#">Fifth</a></li>
			</ul>
		</li>
		<li><a href="#">Service</a>
			<ul>
				<li><a href="#">First</a></li>
				<li><a href="#">Second</a></li>
				<li><a href="#">Third</a></li>
			</ul>
		</li>
		<li><a href="#">Contact</a>
			<ul>
				<li><a href="#">First</a></li>
				<li><a href="#">Second</a></li>
				<li><a href="#">Third</a></li>
				<li><a href="#">Four</a></li>
				<li><a href="#">Fifth</a></li>
			</ul>
		</li>
		<li><a href="#">News</a>
			<ul>
				<li><a href="#">First</a></li>
				<li><a href="#">Second</a></li>
				<li><a href="#">Third</a></li>
				<li><a href="#">Four</a></li>
				<li><a href="#">Fifth</a></li>
				<li><a href="#">six</a></li>
				<li><a href="#">seven</a></li>
			</ul>
		</li>
	</ul>
</body>
</html>

## CSS ##

body{
	background: url('nature.jpg') no-repeat;
	background-size: cover;
	color: #fff;
}
ul {
	margin: 0px;
	padding: 0px;
	list-style: none;
}
ul li{
	float: left;
	width: 220px;
	height: 45px;
	background-color: #000;
	opacity: 0.8;
	text-align: center;
	line-height: 40px;
	font-size: 20px;
}
ul li a{
	text-decoration: none;
	color: #fff;
	display: block;

}
ul li a:hover{
	background-color: blue;
}
ul li ul li{
	display: none;
}
ul li:hover ul li{
	display: block;
}

## Technology Used ##
HTML
CSS